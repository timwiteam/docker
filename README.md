# Dockerfiles & compose

## How to push an image

1. Login to docker Hub (https://hub.docker.com)
2. Go on top of the screen and click on "Create Automated build"
3. If not already done, you'll have to link your docker hub account and your bitbucket account 
** **Use admin-it login to link, not your own account**
4. Complete the repository information (namespace : timwiconsulting, name, etc)
5. Configure the Build Settings to locate the directory
6. Trigger a build of the image

## Existing images

### ionic-v3.1.1

- Ubuntu 16.04 (Trusty)
- Java 8
- Node 8.0.0
- NPM 5.0.1
- Ionic 3.3.0
- Cordova 7.0.1

### angular-cli-v1.0.6

Complete image to dev and build an Angular 4 application with Angular-cli 1.0.6
Image also usable in Jenkins or BitBucket pipelines
Contains :

- Google Chrome for testing
- Sonar Scanner for quality checks integration in Continuous integration

**Versions**

- Node 6.10.2
- Yarn 0.24.5
- Angular-Cli v1.0.6
- Oracle-jdk 8
- Sonar Scanner 3.0.0.702-linux
- Google Chrome 58.0.3029.110

### Mocks with JSON Server

Voir (https://github.com/typicode/json-server)

docker-compose.yml :
```
  mocks:
    build: ./docker/mocks
    tty: true
    stdin_open: true
    hostname: mocks
    ports:
     - "5555:5555"
```

**Versions**

- Node 6.10.2
- Json Server v0.10.1
